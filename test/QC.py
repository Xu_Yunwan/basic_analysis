#!/usr/bin/env python
import sys,os
import time
from itertools import izip
import getopt
import gzip
##test
##parameters
t0=time.time()
#required
fastq1=""
fastq2=""
#optional/default
threshold=33
cutoff=30
out_fq1=''
out_fq2=''
if_gzip=False
fixMisencodedQuals=False

def usage():
    print '''
[usage]: QC-pe.py -t 33(or 64) -c 38 -1 <1.fastq> -2 <2.fastq> -o <1.afterfiltering.fastq> -O <2.afterfiltering.fastq> --fixMisencodedQuals

  options:
        -h/--help    help
    required:
        -1/--fastq1  fastq1
        -2/--fastq2  fastq2
    optional:
        -t/--threshold default:33
        -c/--cutoff  default:30
        -o/--out-fq1 default:"input_file_prefix_1_q%s.fq"%cutoff
        -O/--out-fq2 default:"input_file_prefix_2_q%s.fq"%cutoff
        -z/--gzip-output default:be consistent with input fastq

        --fixMisencodedQuals Qual64to33 default:0 '''

def getParameters(argv):
    if len(argv)==0:
        print "options -1 and -2 are required"
        usage()
        sys.exit(1)
    try:
        opts, args = getopt.getopt(argv,"hzt:c:1:2:o:O:",["help",
                                                  "gzip-output",
                                                  "threshold=",
                                                  "cutoff=",
                                                  "fastq1=",
                                                  "fastq2=",                                                
                                                  "out-fq1=",
                                                  "out-fq2=",
                                                  "fixMisencodedQuals"])
                                                        
    except getopt.GetoptError:
        usage()
        print "[ERROR] getoptError"
        sys.exit(1)
    for opt,arg in opts:
        if opt in ("-h","--help"):
            usage()
            sys.exit(1)
        elif opt in ("-t","--threshold"):
            global threshold
            threshold=int(arg)
        elif opt in ("-c","--cutoff"):
            global cutoff
            cutoff=float(arg)
        elif opt in ("-1","--fastq1"):
            global fastq1
            fastq1=arg
        elif opt in ("-2","--fastq2"):
            global fastq2
            fastq2=arg
        elif opt in ("-o","--out-fq1"):          
            global out_fq1
            out_fq1=arg
        elif opt in ("-O","--out-fq2"):
            global out_fq2
            out_fq2=arg
        elif opt in ("-z","--gzip-output"):
            global if_gzip
            if_gzip=True
        elif opt in ("--fixMisencodedQuals"):
            global fixMisencodedQuals
            fixMisencodedQuals=True

def set_default():
    global out_fq1
    global out_fq2
    if out_fq1=='':
        out_fq1=add_suffix(fastq1,"_q"+str(cutoff))
    if out_fq2=='':
        out_fq2=add_suffix(fastq2,"_q"+str(cutoff))
    if if_gzip==True:
        out_fq1=out_fq1.rstrip('.gz')+'.gz'
        out_fq2=out_fq2.rstrip('.gz')+'.gz'
            
def add_suffix(filename,suffix):
    tail=''
    while os.path.splitext(filename)[1] not in ('.fq','.fastq'):
        tail=os.path.splitext(filename)[1]+tail
        filename=os.path.splitext(filename)[0]
    file_prefix = os.path.splitext(filename)[0]
    file_suffix = os.path.splitext(filename)[1]
    new_filename = file_prefix + suffix + file_suffix + tail
    return new_filename

def read_gz(file_):
    if os.path.splitext(file_)[1]==".gz":
        print "%s is a compressed file"%file_
        with gzip.open(file_) as f:
            for line in f:
                yield line
    else:
        print "%s is a uncompressed file"%file_
        with open(file_) as f:
            for line in f:
                yield line

def open_gz(file_,mode):
    if os.path.splitext(file_)[1]=='.gz':
        f=gzip.open(file_,mode)
    else:
        f=open(file_,mode)
    return f

def Qual64to33(qual):
    new_line=""
    for x in qual:
        if x=='\n':
            new_line+=x
        else:
            Qabs=ord(x)-64
            Q33=Qabs+33
            new_line+=chr(Q33)         
    return new_line

def averQ(qual):
        qual=qual.strip()
        length=len(qual)
        Q=0
        for x in qual:
                Q+=ord(x)-threshold
        if length!=0:
                return Q/length
        else:
                return 0

def QC():
    f10=open(out_fq1,'w')
    f10.close()
    f20=open(out_fq2,'w')
    f20.close()
    f10=open_gz(out_fq1,'a+')
    f20=open_gz(out_fq2,'a+')
    f1=read_gz(fastq1)
    f2=read_gz(fastq2)
    rec1={0:'',1:'',2:'',3:''}
    rec2={0:'',1:'',2:'',3:''}

    lineNum=-1
    for r1,r2 in izip(f1,f2):
        lineNum+=1
        i=lineNum%4
        rec1[i]=r1.strip()
        rec2[i]=r2.strip()

        if i==3:
            if averQ(rec1[3])>=cutoff and averQ(rec2[3])>=cutoff:
                if fixMisencodedQuals==True and threshold==64:
                    rec1[3]=Qual64to33(rec1[3])
                    rec2[3]=Qual64to33(rec2[3])

                f10.write(rec1[0] + '\n')
                f10.write(rec1[1] + '\n')
                f10.write(rec1[2] + '\n')
                f10.write(rec1[3] + '\n')
                
                f20.write(rec2[0] + '\n')
                f20.write(rec2[1] + '\n')
                f20.write(rec2[2] + '\n')
                f20.write(rec2[3] + '\n')

            else:
                pass
        else:
            pass
    f10.close()
    f20.close()

def main(argv):
    getParameters(argv[1:])
    set_default()
    QC()
    print time.time()-t0

if __name__ == "__main__":
	sys.exit(main(sys.argv))
