#!/usr/bin/env python
import fnmatch
import shutil
import os
import time
t0=time.time()
fns=os.listdir('.')
filenames=fnmatch.filter(fns,'x??.gz')
#filenames.sort()
with open('concatenate.fq.gz','wb') as wfp:
    for fn in filenames:
        with open(fn,'rb') as rfp:
            shutil.copyfileobj(rfp,wfp)
print time.time()-t0
