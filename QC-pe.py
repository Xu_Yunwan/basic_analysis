#!/usr/bin/env python
import sys,os
from itertools import izip
import multiprocessing
import getopt
import gzip
import time
import shutil

##parameters

#required
fastq1=""
fastq2=""
global n
n=10
t0=time.time()
#optional/default
threshold=33
cutoff=30
out_fq1=''
out_fq2=''
if_gzip=False
fixMisencodedQuals=False
def usage():
    print '''
[usage]: QC-pe.py -t 33(or 64) -c 38 -1 <1.fastq> -2 <2.fastq> -o <1.afterfiltering.fastq> -O <2.afterfiltering.fastq> --fixMisencodedQuals

  options:
        -h/--help    help
    required:
        -1/--fastq1  fastq1
        -2/--fastq2  fastq2
    optional:
        -t/--threshold default:33
        -c/--cutoff  default:30
        -o/--out-fq1 default:"input_file_prefix_1_q%s.fq"%cutoff
        -O/--out-fq2 default:"input_file_prefix_2_q%s.fq"%cutoff
        -z/--gzip-output default:be consistent with input fastq

        --fixMisencodedQuals Qual64to33 default:0 '''

def getParameters(argv):
    if len(argv)==0:
        print "options -1 and -2 are required"
        usage()
        sys.exit(1)
    try:
        opts, args = getopt.getopt(argv,"hzt:c:1:2:o:O:",["help",
                                                  "gzip-output",
                                                  "threshold=",
                                                  "cutoff=",
                                                  "fastq1=",
                                                  "fastq2=",                                                
                                                  "out-fq1=",
                                                  "out-fq2=",
                                                  "fixMisencodedQuals"])
                                                        
    except getopt.GetoptError:
        usage()
        print "[ERROR] getoptError"
        sys.exit(1)
    for opt,arg in opts:
        if opt in ("-h","--help"):
            usage()
            sys.exit(1)
        elif opt in ("-t","--threshold"):
            global threshold
            threshold=int(arg)
        elif opt in ("-c","--cutoff"):
            global cutoff
            cutoff=float(arg)
        elif opt in ("-1","--fastq1"):
            global fastq1
            fastq1=arg
        elif opt in ("-2","--fastq2"):
            global fastq2
            fastq2=arg
        elif opt in ("-o","--out-fq1"):          
            global out_fq1
            out_fq1=arg
        elif opt in ("-O","--out-fq2"):
            global out_fq2
            out_fq2=arg
        elif opt in ("-z","--gzip-output"):
            global if_gzip
            if_gzip=True
        elif opt in ("--fixMisencodedQuals"):
            global fixMisencodedQuals
            fixMisencodedQuals=True

def set_default():
    global out_fq1
    global out_fq2
    if out_fq1=='':
        out_fq1=add_suffix(fastq1,"_q"+str(cutoff))
    if out_fq2=='':
        out_fq2=add_suffix(fastq2,"_q"+str(cutoff))
    if if_gzip==True:
        out_fq1=out_fq1.rstrip('.gz')+'.gz'
        out_fq2=out_fq2.rstrip('.gz')+'.gz'
            
def add_suffix(filename,suffix):
    tail=''
    while os.path.splitext(filename)[1] not in ('.fq','.fastq'):
        tail=os.path.splitext(filename)[1]+tail
        filename=os.path.splitext(filename)[0]
    file_prefix = os.path.splitext(filename)[0]
    file_suffix = os.path.splitext(filename)[1]
    new_filename = file_prefix + suffix + file_suffix + tail
    return new_filename

def read_gz(file_):
#    print file_
    if os.path.splitext(file_)[1]==".gz":
        print "%s is a compressed file"%file_
        with gzip.open(file_) as f:
            for line in f:
                yield line
    else:
        print "%s is a uncompressed file"%file_
        with open(file_) as f:
            for line in f:
                yield line

def open_gz(file_,mode):
    if os.path.splitext(file_)[1]=='.gz':
        f=gzip.open(file_,mode)
    else:
        f=open(file_,mode)
    return f

def Qual64to33(qual):
    new_line=""
    for x in qual:
        if x=='\n':
            new_line+=x
        else:
            Qabs=ord(x)-64
            Q33=Qabs+33
            new_line+=chr(Q33)         
    return new_line

def averQ(qual):
        qual=qual.strip()
        length=len(qual)
        Q=0
        for x in qual:
                Q+=ord(x)-threshold
        if length!=0:
                return Q/length
        else:
                return 0

class Read():
    def __init__(self,args):
        header,seq,plus_line,qual=args
        self.header=header
        self.seq=seq
        self.plus_line=plus_line
        self.qual=qual
    def __str__(self):
        return '%s\n%s\n%s\n%s\n'%(self.header,self.seq,self.plus_line,self.qual)
class FastqReader():
    def __init__(self,fastq):
        self.tmp_rec=[None]*4
        self.f=read_gz(fastq)
    def __iter__(self):
        return self
    def next(self):
            for i,line in enumerate(self.f):
                self.tmp_rec[i%4]=line.strip()
                if i%4==3:
                    read=Read(self.tmp_rec)
                    if read!=None:
                        return read
            raise StopIteration()

def readfilter((read_pair,f1,f2)):
#    print read_pair , o_file_1, o_file_2
    read1,read2=read_pair
    print f1,f2
    flag='FAIL'
    if averQ(read1.qual)>=cutoff and averQ(read2.qual)>=cutoff:
        flag='PASS'
        if fixMisencodedQuals==True and threshold==64:
            read1.qual=Qual64to33(read1.qual)
            read2.qual=Qual64to33(read2.qual)
        else:
            pass
    if flag=='PASS':
        f1.write(str(read1))
        f2.write(str(read2))
#    return dict(flag=flag,read1=read2,read2=read2)
    
def merge_gz_files(split_files,concatenate_file):
    with open(concatenate_file,'wb') as wfp:
        for fn in split_files:
            with open(fn,'rb') as rfp:
                shutil.copyfileobj(rfp,wfp)
    for file_ in split_files:
        os.remove(file_)

def QC():
#    f10=open(out_fq1,'w')
#    f10.close()
#    f20=open(out_fq2,'w')
#    f20.close()
#    f10=open_gz(out_fq1,'a+')
#    f20=open_gz(out_fq2,'a+')
    tmp_files_1=[]
    tmp_files_2=[]
    fd1=[]
    fd2=[]
    for i in range(n):
        tmp_files_1+=[add_suffix(out_fq1,'.tmp0000%s'%i)]
        tmp_files_2+=[add_suffix(out_fq2,'.tmp0000%s'%i)]
    for file1,file2 in izip(tmp_files_1,tmp_files_2):
        f1=open(file1,'w')
        f2=open(file2,'w')
        f1.close()
        f2.close()
        fd1+=[open(file1,'a+')]
        fd2+=[open(file2,'a+')]
#    print tmp_files_1
#    print tmp_files_2
    pool=multiprocessing.Pool(processes = n)          
    arg_ReadList=[]
#    Notes=\
#'''
    for i,(read1,read2) in enumerate(izip(FastqReader(fastq1),FastqReader(fastq2))):
        arg_ReadList+=[(read1,read2)]
        if i%n==n-1:    
            #print arg_ReadList
#            print arg_ReadList
            results=pool.map(readfilter,list(zip(arg_ReadList,fd1,fd2)))   
   #         print results
#            for result in results:
#                print result
#                if result['flag']=='PASS':
#                    f10.write(str(result['read1']))
#                    f20.write(str(result['read2']))
            arg_ReadList=[]  
#    f10.close()
#    f20.close()
#'''
    for f1,f2 in izip(fd1,fd2):
        f1.close()
        f2.close()
    merge_gz_files(tmp_files_1,out_fq1)
    merge_gz_files(tmp_files_2,out_fq2)
def main(argv):
    t0=time.time()
    getParameters(argv[1:])
    set_default()
    QC()
    print time.time()-t0

if __name__ == "__main__":
	sys.exit(main(sys.argv))
