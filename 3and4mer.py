#!/usr/bin/env python

import sys
import getopt
import os
import gzip
import xlrd
import xlwt
from xlutils.copy import copy

def usage():
        print """
        -h --help
        -k --kmerLengthList    e.g. "1,3,4"
        -q --fastq
        -r --reference
        -o --outputExcel       e.g. "example.xls"

        2017/11/16
          """

#parameters
fastq=""
reference=""
outputExcel=""
k=0


def getParameters(argv):
    try:
        opts, args = getopt.getopt(argv,"hk:q:r:o:",["help",
                                                  "kmerLengthList=",
                                                  "fastq=",
                                                  "reference=",
                                                  "outputExcel="])
    except getopt.GetoptError:
        usage()
        print "[ERROR] getoptError"
        sys.exit(1)
    for opt,arg in opts:
        if opt in ("-h","--help"):
            usage()
            sys.exit(1)
        elif opt in ("-k","--kmerLengthList"):
            global k
            k=arg
        elif opt in ("-q","--fastq"):
            global fastq
            fastq=arg
        elif opt in ("-r","--reference"):
            global reference
            reference=arg
        elif opt in ("-o","--outputExcel"):
            global outputExcel
            outputExcel=arg


# -- tools -- #

def read_gz(file_):
        if os.path.splitext(file_)[1]==".gz":
                with gzip.open(file_) as f:
                        for line in f:
                                yield line
        else:
                with open(file_) as f:
                        for line in f:
                                yield line

baseDict = {"A": "0", "T": "1", "C": "2", "G": "3",
                                "0": "A", "1": "T", "2": "C", "3": "G"}
def uniformize(D):
        def SumDictValue(dict):
                SUM = 0.0001
                for key in dict:
                        SUM += dict[key]
                return SUM
        SUM=SumDictValue(D)
        for key in D:
                D[key] = D[key] / float(SUM)
        return D

        #for key in kmerDict:
        #       kmerDict[key] = kmerDict[key] / float(Sum(kmerDict))#################!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

def int2kmer(int_n,k):
        def trans_10_to_4(a):
                tmp = ''
                a = int(a)
                while (1):
                        x = a % 4
                        tmp = str(x) + tmp
                        if (a / 4 == 0):
                                break
                        a = a / 4
                return tmp  # str

        str_n = trans_10_to_4(int_n)
        n = str_n.zfill(k)
        kmer = ""
        for x in n:
                kmer += baseDict[x]
        return kmer
def kmer2int(kmer):
        encoded_kmer=""
        for base in kmer:
                encoded_kmer+=baseDict[base]
        return int(encoded_kmer,4)
def write_excel_col(col,col_n,xslfile):
        xslfile=xslfile.decode('utf-8')
        if os.path.exists(xslfile):
                wb = copy(xlrd.open_workbook(xslfile,on_demand=True))
        else:
                wb = xlwt.Workbook(xslfile)
                ws = wb.add_sheet(u"sheet1")
        for row_n in range(len(col)):
                wb.get_sheet(0).write(row_n,col_n,col[row_n])
        wb.save(xslfile)
def DictSetdefault(k):
        D={}
        for i in range(4**k):
                kmer=int2kmer(i,k)
                D.setdefault(kmer,0)
        return D
def DictToList(dict):
        list=[0.0]*len(dict)
        for kmer_key in dict:
                list[kmer2int(kmer_key)]=dict[kmer_key]
        return list
# ----------- #


def cal_freq_in_read_both_end(fastq,k):
        kmer5=""
        kmer3=""
        kmerDict3=DictSetdefault(k)
        kmerDict5=DictSetdefault(k)
        f=read_gz(fastq)
        if True:
                for idx, line in enumerate(f):
                        i = idx % 4
                        if i == 1 and len(line.strip()) <= 101:
                                line = line.strip()
                                kmer5 = line[:k]
                                try :
                                        kmerDict5[kmer5] += 1
                                except KeyError:
                                        pass
                                kmer3 = line[-k:]
                                try :
                                        kmerDict3[kmer3] += 1
                                except KeyError:
                                        pass

        return DictToList(uniformize(kmerDict3)),DictToList(uniformize(kmerDict5))

def GenomeBackground(reference,k):
        kmer = ""
        D = DictSetdefault(k)
        f=read_gz(fastq)
        if True:
                head = ""
                for line in f:
                        if line[0] != ">":
                                line = line.strip()
                                line = head + line
                                for pos in range(len(line)-k+1):
                                        kmer = line[pos:pos+k]
                                        #D.setdefault(kmer, 0)#!
                                        try :
                                                D[kmer] += 1
                                        except KeyError:
                                                pass

                                head = line[len(line)-k+1:]
        return DictToList(uniformize(D))


def main(argv):
        getParameters(argv[1:])

        if fastq=="" or reference=="" or outputExcel=="" or k=="" :
                usage()
                print "getopt error"
                exit(1)
        else:
                output_excel=outputExcel

        for i,k_ in enumerate(k.split(",")):
                kmerList=[]
                k_=int(k_)
                for ii in range(4**k_):
                        kmerList+=[int2kmer(ii,k_)]
                genomeBackground = GenomeBackground(reference,k_)
                endFreq3 , endFreq5 = cal_freq_in_read_both_end(fastq,k_)
                m=str(k_)+"MER_LIST"
                kmerList=[m]+kmerList
                print genomeBackground
                print endFreq3
                genomeBackground=["GENOME_BACKGROUND"]+genomeBackground
                endFreq3=["3_END_FREQ"]+endFreq3
                endFreq5=["5_END_FREQ"]+endFreq5

                write_excel_col(kmerList,i*4+0,output_excel)
                write_excel_col(genomeBackground,i*4+1,output_excel)
                write_excel_col(endFreq3,i*4+2,output_excel)
                write_excel_col(endFreq5,i*4+3,output_excel)

if __name__ == '__main__':
    sys.exit(main(sys.argv))
