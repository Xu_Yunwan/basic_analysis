# CtDNA QC Report
[ TOC ]

## General Stats

## Fastq Stats
### lane_1
#### Sequence Length Distribution

<table><tr>
<td><img id="Sequence Length Distribution raw 1" border=0></td>
<td><img id="Sequence Length Distribution clean 1" border=0></td>
</tr></table>

#### Sequence Duplication Levels
<table><tr>
<td><img id="Sequence Duplication Levels raw 1" border=0></td>
<td><img id="Sequence Duplication Levels clean 1" border=0></td>
</tr></table>
#### Per Sequence Quality Scores

<table><tr>
<td><img id="Per Sequence Quality Scores raw 1" border=0></td>
<td><img id="Per Sequence Quality Scores clean 1" border=0></td>
</tr></table>
#### Per Sequence GC Content

<table><tr>
<td><img id="Per Sequence GC Content raw 1" border=0></td>
<td><img id="Per Sequence GC Content clean 1" border=0></td>
</tr></table>
#### Mean Quality Scores

<table><tr>
<td><img id="Mean Quality Scores raw 1" border=0></td>
<td><img id="Mean Quality Scores clean 1" border=0></td>
</tr></table>
#### Per Base N Content

<table><tr>
<td><img id="Per Base N Content raw 1" border=0></td>
<td><img id="Per Base N Content clean 1" border=0></td>
</tr></table>

#### Overrepresented sequences

<table><tr>
<td><img id="Overrepresented sequences raw 1" border=0></td>
<td><img id="Overrepresented sequences clean 1" border=0></td>
</tr></table>

#### Coverage histogram

<table><tr>
<td><img id="Coverage histogram 1" border=0></td>
</tr></table>

#### Genome fraction

<table><tr>
<td><img id="Genome fraction 1" border=0></td>
</tr></table>

#### Insert size histogram

<table><tr>
<td><img id="Insert size histogram 1" border=0></td>
</tr></table>

#### GC content

<table><tr>
<td><img id="GC content 1" border=0></td>
</tr></table>

### lane_2
#### Sequence Length Distribution

<table><tr>
<td><img id="Sequence Length Distribution raw 2" border=0></td>
<td><img id="Sequence Length Distribution clean 2" border=0></td>
</tr></table>


#### Sequence Duplication Levels

<table><tr>
<td><img id="Sequence Duplication Levels raw 2" border=0></td>
<td><img id="Sequence Duplication Levels clean 2" border=0></td>
</tr></table>

#### Per Sequence Quality Scores

<table><tr>
<td><img id="Per Sequence Quality Scores raw 2" border=0></td>
<td><img id="Per Sequence Quality Scores clean 2" border=0></td>
</tr></table>

#### Per Sequence GC Content

<table><tr>
<td><img id="Sequence Duplication Levels raw 2" border=0></td>
<td><img id="Sequence Duplication Levels clean 2" border=0></td>
</tr></table>

#### Mean Quality Scores

<table><tr>
<td><img id="Mean Quality Scores raw 2" border=0></td>
<td><img id="Mean Quality Scores clean 2" border=0></td>
</tr></table>

#### Per Base N Content

<table><tr>
<td><img id="Per Base N Content raw 2" border=0></td>
<td><img id="Per Base N Content clean 2" border=0></td>
</tr></table>
#### Overrepresented sequences

<table><tr>
<td><img id="Overrepresented sequences raw 2" border=0></td>
<td><img id="Overrepresented sequences clean 2" border=0></td>
</tr></table>
#### Coverage histogram

<table><tr>
<td><img id="Coverage histogram 2" border=0></td>
</tr></table>
#### Genome fraction
<table><tr>
<td><img id="Genome fraction 2" border=0></td>
</tr></table>
#### Insert size histogram

<table><tr>
<td><img id="Insert size histogram 2" border=0></td>
</tr></table>
#### GC content

<table><tr>
<td><img id="GC content 2" border=0></td>
</tr></table>
### lane_3
#### Sequence Length Distribution

<table><tr>
<td><img id="Sequence Length Distribution raw 3" border=0></td>
<td><img id="Sequence Length Distribution clean 3" border=0></td>
</tr></table>

#### Sequence Duplication Levels
<table><tr>
<td><img id="Sequence Duplication Levels raw 3" border=0></td>
<td><img id="Sequence Duplication Levels clean 3" border=0></td>
</tr></table>
#### Per Sequence Quality Scores

<table><tr>
<td><img id="Per Sequence Quality Scores raw 3" border=0></td>
<td><img id="Per Sequence Quality Scores clean 3" border=0></td>
</tr></table>
#### Per Sequence GC Content

<table><tr>
<td><img id="Per Sequence GC Content raw 3" border=0></td>
<td><img id="Per Sequence GC Content clean 3" border=0></td>
</tr></table>
#### Mean Quality Scores

<table><tr>
<td><img id="Mean Quality Scores raw 3" border=0></td>
<td><img id="Mean Quality Scores clean 3" border=0></td>
</tr></table>
#### Per Base N Content

<table><tr>
<td><img id="Per Base N Content raw 3" border=0></td>
<td><img id="Per Base N Content clean 3" border=0></td>
</tr></table>
#### Overrepresented sequences

<table><tr>
<td><img id="Overrepresented sequences raw 3" border=0></td>
<td><img id="Overrepresented sequences clean 3" border=0></td>
</tr></table>
#### Coverage histogram

<table><tr>
<td><img id="Coverage histogram 3" border=0></td>
</tr></table>
#### Genome fraction
<table><tr>
<td><img id="Genome fraction 3" border=0></td>
</tr></table>
#### Insert size histogram

<table><tr>
<td><img id="Insert size histogram 3" border=0></td>
</tr></table>
#### GC content

<table><tr>
<td><img id="GC content 3" border=0></td>
</tr></table>
### lane_4
#### Sequence Length Distribution

<table><tr>
<td><img id="Sequence Length Distribution raw 4" border=0></td>
<td><img id="Sequence Length Distribution clean 4" border=0></td>
</tr></table>

#### Sequence Duplication Levels
<table><tr>
<td><img id="Sequence Duplication Levels raw 4" border=0></td>
<td><img id="Sequence Duplication Levels clean 4" border=0></td>
</tr></table>
#### Per Sequence Quality Scores

<table><tr>
<td><img id="Per Sequence Quality Scores raw 4" border=0></td>
<td><img id="Per Sequence Quality Scores clean 4" border=0></td>
</tr></table>
#### Per Sequence GC Content

<table><tr>
<td><img id="Per Sequence GC Content raw 4" border=0></td>
<td><img id="Per Sequence GC Content clean 4" border=0></td>
</tr></table>
#### Mean Quality Scores

<table><tr>
<td><img id="Mean Quality Scores raw 4" border=0></td>
<td><img id="Mean Quality Scores clean 4" border=0></td>
</tr></table>
#### Per Base N Content

<table><tr>
<td><img id="Per Base N Content raw 4" border=0></td>
<td><img id="Per Base N Content clean 4" border=0></td>
</tr></table>
#### Overrepresented sequences

<table><tr>
<td><img id="Overrepresented sequences raw 4" border=0></td>
<td><img id="Overrepresented sequences clean 4" border=0></td>
</tr></table>
#### Coverage histogram

<table><tr>
<td><img id="Coverage histogram 4" border=0></td>
</tr></table>
#### Genome fraction
<table><tr>
<td><img id="Genome fraction 4" border=0></td>
</tr></table>
#### Insert size histogram

<table><tr>
<td><img id="Insert size histogram 4" border=0></td>
</tr></table>
#### GC content

<table><tr>
<td><img id="GC content 4" border=0></td>
</tr></table>
### lane_5
#### Sequence Length Distribution

<table><tr>
<td><img id="Sequence Length Distribution raw 5" border=0></td>
<td><img id="Sequence Length Distribution clean 5" border=0></td>
</tr></table>

#### Sequence Duplication Levels
<table><tr>
<td><img id="Sequence Duplication Levels raw 5" border=0></td>
<td><img id="Sequence Duplication Levels clean 5" border=0></td>
</tr></table>
#### Per Sequence Quality Scores

<table><tr>
<td><img id="Per Sequence Quality Scores raw 5" border=0></td>
<td><img id="Per Sequence Quality Scores clean 5" border=0></td>
</tr></table>
#### Per Sequence GC Content

<table><tr>
<td><img id="Per Sequence GC Content raw 5" border=0></td>
<td><img id="Per Sequence GC Content clean 5" border=0></td>
</tr></table>
#### Mean Quality Scores

<table><tr>
<td><img id="Mean Quality Scores raw 5" border=0></td>
<td><img id="Mean Quality Scores clean 5" border=0></td>
</tr></table>

#### Per Base N Content

<table><tr>
<td><img id="Per Base N Content raw 5" border=0></td>
<td><img id="Per Base N Content clean 5" border=0></td>
</tr></table>
#### Overrepresented sequences

<table><tr>
<td><img id="Overrepresented sequences raw 5" border=0></td>
<td><img id="Overrepresented sequences clean 5" border=0></td>
</tr></table>
#### Coverage histogram

<table><tr>
<td><img id="Coverage histogram 5" border=0></td>
</tr></table>
#### Genome fraction
<table><tr>
<td><img id="Genome fraction 5" border=0></td>
</tr></table>
#### Insert size histogram

<table><tr>
<td><img id="Insert size histogram 5" border=0></td>
</tr></table>
#### GC content

<table><tr>
<td><img id="GC content 5" border=0></td>
</tr></table>

<script>

////////lane 1
document.getElementById("Sequence Length Distribution raw 1").src=""
document.getElementById("Sequence Length Distribution clean 1").src=""
document.getElementById("Sequence Duplication Levels raw 1").src=""
document.getElementById("Sequence Duplication Levels clean 1").src=""

document.getElementById("Per Sequence Quality Scores raw 1").src=""
document.getElementById("Per Sequence Quality Scores clean 1").src=""
document.getElementById("Per Sequence GC Content raw 1").src=""
document.getElementById("Per Sequence GC Content clean 1").src=""

document.getElementById("Mean Quality Scores raw 1").src=""
document.getElementById("Mean Quality Scores clean 1").src=""

document.getElementById("Per Base N Content raw 1").src=""
document.getElementById("Per Base N Content clean 1").src=""

document.getElementById("Overrepresented sequences raw 1").src=""
document.getElementById("Overrepresented sequences clean 1").src=""
document.getElementById("Coverage histogram 1").src=""
document.getElementById("Genome fraction 1").src=""
document.getElementById("Insert size histogram 1").src=""
document.getElementById("GC content 1").src=""
////////////lane 2
document.getElementById("Sequence Length Distribution raw 2").src=""
document.getElementById("Sequence Length Distribution clean 2").src=""
document.getElementById("Sequence Duplication Levels raw 2").src=""
document.getElementById("Sequence Duplication Levels clean 2").src=""

document.getElementById("Per Sequence Quality Scores raw 2").src=""
document.getElementById("Per Sequence Quality Scores clean 2").src=""
document.getElementById("Per Sequence GC Content raw 2").src=""
document.getElementById("Per Sequence GC Content clean 2").src=""

document.getElementById("Mean Quality Scores raw 2").src=""
document.getElementById("Mean Quality Scores clean 2").src=""

document.getElementById("Per Base N Content raw 2").src=""
document.getElementById("Per Base N Content clean 2").src=""

document.getElementById("Overrepresented sequences raw 2").src=""
document.getElementById("Overrepresented sequences clean 2").src=""
document.getElementById("Coverage histogram 2").src=""
document.getElementById("Genome fraction 2").src=""
document.getElementById("Insert size histogram 2").src=""
document.getElementById("GC content 2").src=""
////////////lane 3
document.getElementById("Sequence Length Distribution raw 3").src=""
document.getElementById("Sequence Length Distribution clean 3").src=""
document.getElementById("Sequence Duplication Levels raw 3").src=""
document.getElementById("Sequence Duplication Levels clean 3").src=""

document.getElementById("Per Sequence Quality Scores raw 3").src=""
document.getElementById("Per Sequence Quality Scores clean 3").src=""
document.getElementById("Per Sequence GC Content raw 3").src=""
document.getElementById("Per Sequence GC Content clean 3").src=""

document.getElementById("Mean Quality Scores raw 3").src=""
document.getElementById("Mean Quality Scores clean 3").src=""

document.getElementById("Per Base N Content raw 3").src=""
document.getElementById("Per Base N Content clean 3").src=""

document.getElementById("Overrepresented sequences raw 3").src=""
document.getElementById("Overrepresented sequences clean 3").src=""
document.getElementById("Coverage histogram 3").src=""
document.getElementById("Genome fraction 3").src=""
document.getElementById("Insert size histogram 3").src=""
document.getElementById("GC content 3").src=""
////////////lane 4
document.getElementById("Sequence Length Distribution raw 4").src=""
document.getElementById("Sequence Length Distribution clean 4").src=""
document.getElementById("Sequence Duplication Levels raw 4").src=""
document.getElementById("Sequence Duplication Levels clean 4").src=""

document.getElementById("Per Sequence Quality Scores raw 4").src=""
document.getElementById("Per Sequence Quality Scores clean 4").src=""
document.getElementById("Per Sequence GC Content raw 4").src=""
document.getElementById("Per Sequence GC Content clean 4").src=""

document.getElementById("Mean Quality Scores raw 4").src=""
document.getElementById("Mean Quality Scores clean 4").src=""

document.getElementById("Per Base N Content raw 4").src=""
document.getElementById("Per Base N Content clean 4").src=""

document.getElementById("Overrepresented sequences raw 4").src=""
document.getElementById("Overrepresented sequences clean 4").src=""
document.getElementById("Coverage histogram 4").src=""
document.getElementById("Genome fraction 4").src=""
document.getElementById("Insert size histogram 4").src=""
document.getElementById("GC content 4").src=""
////////////lane 5
document.getElementById("Sequence Length Distribution raw 5").src=""
document.getElementById("Sequence Length Distribution clean 5").src=""
document.getElementById("Sequence Duplication Levels raw 5").src=""
document.getElementById("Sequence Duplication Levels clean 5").src=""

document.getElementById("Per Sequence Quality Scores raw 5").src=""
document.getElementById("Per Sequence Quality Scores clean 5").src=""
document.getElementById("Per Sequence GC Content raw 5").src=""
document.getElementById("Per Sequence GC Content clean 5").src=""

document.getElementById("Mean Quality Scores raw 5").src=""
document.getElementById("Mean Quality Scores clean 5").src=""

document.getElementById("Per Base N Content raw 5").src=""
document.getElementById("Per Base N Content clean 5").src=""

document.getElementById("Overrepresented sequences raw 5").src=""
document.getElementById("Overrepresented sequences clean 5").src=""
document.getElementById("Coverage histogram 5").src=""
document.getElementById("Genome fraction 5").src=""
document.getElementById("Insert size histogram 5").src=""
document.getElementById("GC content 5").src=""

</script>

