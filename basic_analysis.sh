set -e
all_core_number=30
sample_number=2
cfg_file=$1
bin=$2
bin_path=`readlink -f $bin`
qualimap_path="/data/home/xuyw/software/qualimap/qualimap_v2.2.1/"
PATH=$bin_path:$qualimap_path:$PATH
export PATH

function all(){
        sampleID=$1 fastq1=$2 fastq2=$3 reference=$4
##step1: fastq qc
#echo $1 : $2 : $3 : $4
##exit 1
        fastqc -t 15 $fastq1 ./ &
        fastqc -t 15 $fastq2 ./
        wait
        fastq1_after_processing=${sampleID}_afterFiltering_1.fastq
        fastq2_after_processing=${sampleID}_afterFiltering_2.fastq
        QC-pe.py -1 $fastq1 -2 $fastq2 -o $fastq1_after_processing -O $fastq2_after_processing
        fastqc -t 15 $fastq1_after_processing ./ &
        fastqc -t 15 $fastq2_after_processing ./
        wait
##step2: bam qc
##      bwamem -t `expr $all_core_number / $sample_number`  -r $reference -1 $fastq1_after_processing -2 $fastq2_after_processing -f ${sampleID}.sam
        processDNA.sh $fastq1_after_processing $fastq2_after_processing $sampleID
        bamfile=${sampleID}.final.bam
        qualimap bamqc -bam $bamfile -outdir ./qualimap_out
}
IFS=$'\n'
for file_zip in `readcfg $cfg_file sample`
do
        IFS=$' '
        all $file_zip
done
set +e
